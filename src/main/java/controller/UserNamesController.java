package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import model.ResponseResults;
import services.UserNamesService;

@RestController
public class UserNamesController {

	@Autowired
	private UserNamesService userNamesService;

	@RequestMapping(value = "/nombres", consumes = "application/json", method = RequestMethod.POST)
	public ResponseEntity<ResponseResults> ResponseResults(@RequestBody List<String> nombresinput) {
		
		ResponseResults coso = userNamesService.coso(nombresinput);
		
		return ResponseEntity.ok(coso);
	}
}